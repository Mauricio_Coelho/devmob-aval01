const staticDevSB = "dev-sb-app-vl"
const assets =[
    "src/index.html",
    "src/js/sb-admin-2.min.js",
    "src/js/sb-admin-2.js",
    "src/css/sb-admin-2.min.css",
    "src/css/sb-admin-2.css",
    "src/img/undraw_posting_photo",
    "src/img/undraw_profile.svg",
    "src/img/undraw_profile_1.svg",
    "src/img/undraw_profile_2.svg",
    "src/img/undraw_profile_3.svg",
    "src/img/undraw_rocket",
]

self.addEventListener("install", installEvent =>{
    installEvent.waitUntil(
        caches.open(staticDevSB).then(cache => {
            cache.addAll(assets)
        })
    )
})

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
      caches.match(fetchEvent.request).then((resp) => {
        return resp || fetch(fetchEvent.request).then((response) => {
          return caches.open(staticDevSB).then((cache) => {
            cache.put(fetchEvent.request, response.clone());
            return response;
          });
        });
      })
    );
  });
